import React from 'react'

import './Header.css'

import logo from './assets/hc-logo.svg'

export default function Header() {
  return (
    <header>
      <nav>
        <a href="/" title="Go to home page">
          <h1>
            <img src={logo} alt="HolidayCheck"/>
          </h1>
        </a>

        <ul>
          <li><a href="/"><span>Dashboard</span></a></li>
          <li><a href="/" className="active"><span>Reviews</span></a></li>
          <li><a href="/"><span>Hotel Manager</span></a></li>
          <li><a href="/"><span>Settings</span></a></li>
        </ul>
      </nav>
    </header>
  )
}
