import React from 'react'

import './Footer.css'

export default function Footer() {
  return (
    <footer>
      <p>&copy; 1999 - {new Date().getFullYear()} HolidayCheck AG</p>
    </footer>
  )
}
