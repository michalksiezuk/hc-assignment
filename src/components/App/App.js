import React, {Component} from 'react'

import Header from '../Header/Header'
import Reviews from '../Reviews/Reviews'
import Footer from '../Footer/Footer'

// Dummy data
import data from '../../data/reviews.json'
import loggedInUser from '../../data/loggedin-user.json'

import './App.css'

export default class App extends Component {
  render() {
    return (
      <div className="page reviews">
        <Header/>
        <Reviews reviewsData={data} loggedInUser={loggedInUser}/>
        <Footer/>
      </div>
    )
  }
}
