import React, {Component} from 'react'
import PropTypes from 'prop-types'

import './UserAvatar.css'

export default class UserAvatar extends Component {
  render() {
    return (
      <figure className={`user-avatar ${this.props.placement}`}>
        <img src={this.props.userImage} alt={this.props.userName}/>
        <figcaption>
          {this.props.userName}
          {this.props.userTitle && this.props.userTitle !== '' && <small>{this.props.userTitle}</small>}
        </figcaption>
      </figure>
    )
  }
}

UserAvatar.defaultProps = {
  placement: 'review-header'
};

UserAvatar.propTypes = {
  placement: PropTypes.oneOf(['review-header', 'comment-footer']).isRequired,
  userName: PropTypes.string.isRequired,
  userTitle: PropTypes.string,
  userImage: function(props, propName, componentName) {
    if(!/^(http|https):\/\/[^ "]+$/.test(props[propName])) {
      return new Error(`Invalid prop ${propName} supplied to ${componentName}. Should be valid URL.`)
    }
  }
};
