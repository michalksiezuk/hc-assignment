import React, {Component} from 'react'

import './CollapsibleText.css'

export default class CollapsibleText extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bodyVisible: false,
      bodyHeight: null
    }
  }

  bodyRef(ref) {
    this.containerNode = ref
  }

  measureBody() {
    this.setState({
      bodyHeight: this.containerNode.clientHeight
    })
  }

  expandBodyText() {
    this.setState({
      bodyVisible: true
    })
  }

  collapseBodyText() {
    this.setState({
      bodyVisible: false
    })
  }

  componentDidMount() {
    this.measureBody()
  }

  render() {
    const needsExpanding = this.state.bodyHeight < 98;
    const expandedClass = this.state.bodyVisible || needsExpanding ? 'expanded' : '';

    return (
      <div className={`collapsible-text ${expandedClass}`}>
        <p onClick={() => this.expandBodyText()} ref={ref => this.bodyRef(ref)}>
          {this.props.bodyText}
        </p>
        <button onClick={() => this.collapseBodyText()} className={needsExpanding ? 'hidden' : ''}>
          Show less &uarr;
        </button>
      </div>
    )
  }
}
