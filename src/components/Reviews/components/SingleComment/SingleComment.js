import React, {Component} from 'react'
import UserAvatar from '../UserAvatar/UserAvatar'
import CollapsibleText from '../CollapsibleText/CollapsibleText'

import './SingleComment.css'

export default class SingleComment extends Component {
  render() {
    const {commentData} = this.props;

    return (
      <div className="single-comment">
        <h3>Comment</h3>
        <CollapsibleText bodyText={commentData.data.body}/>
        <footer>
          <UserAvatar userImage={commentData.author.avatar} userName={commentData.author.name}
                      userTitle={commentData.author.title} placement="comment-footer"/>
        </footer>
      </div>
    )
  }
}
