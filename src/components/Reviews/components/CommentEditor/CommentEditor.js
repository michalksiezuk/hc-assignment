import React, {Component} from 'react'

import './CommentEditor.css'

export default class CommentEditor extends Component {
  constructor() {
    super();

    this.state = {
      value: ''
    }
  }

  handleTextAreaChange(event) {
    this.setState({
      value: event.target.value
    })
  }

  onSaveHandler() {
    this.props.saveButtonHandler(this.state.value, this.props.loggedInUser)
  }

  render() {
    return (
      <div className="comment-editor">
        <h3>Add comment</h3>
        <textarea value={this.state.value} onChange={event => this.handleTextAreaChange(event)}/>
        <div className="actions">
          <button className="action-button cancel-edits" onClick={this.props.cancelButtonHandler}>Cancel</button>
          <button className="action-button save-edits" onClick={() => this.onSaveHandler()}
                  disabled={this.state.value === ''}>Save</button>
        </div>
      </div>
    )
  }
}
