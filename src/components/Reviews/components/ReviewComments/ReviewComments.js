import React, {Component} from 'react'

import SingleComment from '../SingleComment/SingleComment'
import CommentEditor from '../CommentEditor/CommentEditor'

import './ReviewComments.css'

export default class ReviewComments extends Component {
  constructor() {
    super();

    this.state = {
      isEditing: false,
      comments: []
    }
  }

  commentEditor() {
    return (
      <CommentEditor
        saveButtonHandler={(value, user) => this.saveButtonHandler(value, user)}
        cancelButtonHandler={() => this.cancelButtonHandler()}
        loggedInUser={this.props.loggedInUser}/>
    )
  }

  showCommentEditor() {
    this.setState({
      isEditing: true
    })
  }

  saveButtonHandler(value, user) {
    this.setState({
      isEditing: false,
      comments: [
        ...this.state.comments,
        {
          "id": 0,
          "created": Date.now(),
          "author": {
            "name": user.name,
            "avatar": user.avatar,
            "title": user.title
          },
          "data": {
            "body": value
          }
        }
      ]
    })
  }

  cancelButtonHandler() {
    this.setState({
      isEditing: false
    })
  }

  render() {
    const editor = this.state.isEditing && this.commentEditor();

    const classes = [
      this.state.isEditing ? 'is-editing' : '',
      this.state.comments.length > 0 ? 'has-comments' : ''
    ].join(' ');

    const comments = this.state.comments.map(
      data => <SingleComment key={data.id} commentData={data}/>
    );

    return (
      <div className={`comments-wrapper ${classes}`}>
        {comments}
        {editor}
        <button className="action-button add-comment" onClick={() => this.showCommentEditor()}>Add comment</button>
      </div>
    )
  }
}
