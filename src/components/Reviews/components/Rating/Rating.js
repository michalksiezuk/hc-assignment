import React, {Component} from 'react'

import './Rating.css'

export default class Rating extends Component {
  render() {
    let satisfied = this.props.satisfied ? 'thumbs-up' : 'thumbs-down';
    let stars = [];

    for (let i = 0; i < 6; i++) {
      stars.push(
        <i key={i} className={`fas fa-star ${this.props.rating > i ? 'active' : ''}`}/>)
    }

    return (
      <div className="review-rating">
        <div className={`satisfied ${satisfied}`}>
          <i className={`fas fa-${satisfied}`}/>
        </div>
        <div className="rating-stars">
          {stars}
        </div>
        <div className="rating-value">{`${this.props.rating}/6`}</div>
      </div>
    )
  }
}
