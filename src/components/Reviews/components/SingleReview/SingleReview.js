import React, {Component} from 'react'
import Moment from 'moment'

import UserAvatar from '../UserAvatar/UserAvatar'
import Rating from '../Rating/Rating'
import CollapsibleText from '../CollapsibleText/CollapsibleText'
import ReviewComments from '../ReviewComments/ReviewComments'

import './SingleReview.css'

export default class SingleReview extends Component {
  render() {
    const {review} = this.props;
    const created = Moment(review.created).subtract(42 * review.id, 'days').format('D MMMM YYYY');

    return (
      <article className="single-review">
        <header>
          <UserAvatar userImage={review.author.avatar} userName={review.author.name}/>
          <div className="created">{created}</div>
          <h2>{review.data.title}</h2>
        </header>
        <Rating satisfied={review.data.satisfied} rating={review.data.rating}/>
        <CollapsibleText bodyText={review.data.body}/>
        <ReviewComments loggedInUser={this.props.loggedInUser}/>
      </article>
    )
  }
}
