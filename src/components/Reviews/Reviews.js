import React, {Component} from 'react'

import SingleReview from './components/SingleReview/SingleReview'

import './Reviews.css'

export default class Reviews extends Component {
  render() {
    const reviews = this.props.reviewsData.map(
      review => <SingleReview key={review.id} review={review} loggedInUser={this.props.loggedInUser}/>
    );

    return (
      <main>
        <div className="reviews-wrapper">
          {reviews}
        </div>
      </main>
    )
  }
}
