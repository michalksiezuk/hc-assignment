# HolidayCheck - Frontend Developer assignment

Author: Michał Księżuk

Header photo by Recal Media from Pexels [link](https://www.pexels.com/photo/beach-bench-boardwalk-clouds-462024/)

### Installation

```bash
git clone git@bitbucket.org:michalksiezuk/hc-assignment.git
cd hc-assignment
npm i
npm start
```
